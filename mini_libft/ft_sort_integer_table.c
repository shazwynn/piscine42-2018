/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 01:31:22 by algrele           #+#    #+#             */
/*   Updated: 2018/02/08 13:11:42 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	my_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_sort_integer_table(int *tab, int size)
{
	int i;
	int d;

	d = 1;
	while (d > 0)
	{
		d = 0;
		i = 0;
		while (i < size - 1)
		{
			if (tab[i] > tab[i + 1])
			{
				my_swap(&tab[i], &tab[i + 1]);
				d++;
			}
			i++;
		}
		size--;
	}
}
