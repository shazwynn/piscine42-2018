#ifndef MINILFT_H
# define MINILFT_H

#include <unistd.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nb);
int		ft_strlen(char *str);
char	*ft_strrev(char *str);
int		ft_atoi(char *str);
void	ft_sort_integer_table(int *tab, int size);
void	ft_div_mod(int a, int b, int *div, int *mod);
void	ft_ft(int *nbr);
void	ft_swap(int *a, int *b);
void	ft_ultimate_div_mod(int *a, int *b);

#endif
