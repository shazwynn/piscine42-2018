/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 16:39:15 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 16:48:28 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>



char	*ft_itoa(int nbr)
{
	int len;
	int i;
	char	*array;

	len = 1;
	if (nbr < 0)
		nbr = -nbr;
	i = nbr;
	while (i >= 10)
	{
		len++;
		i = i / 10;
	}
	array = (char *)malloc(sizeof(char) * (len + 2));
	if (!array)
		return (NULL);
	i = 0;
	while (i < len + 2)
		array[i] = '\0';
	ft_rec_itoa(array, nbr);
	return (array);
}

int	main(void)
{
	ft_itoa(0);
	printf("\n");
	ft_itoa(12);
	printf("\n");
	ft_itoa(2147483647);
	return (0);
}
