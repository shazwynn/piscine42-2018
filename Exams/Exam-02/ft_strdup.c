/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 13:40:10 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 13:43:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *dest, char *src)
{
	int		i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}


char	*ft_strdup(char *src)
{
	int		len;
	char *dest;

	len = 0;
	while (src[len])
		len++;
	dest = (char *)malloc((sizeof(char)) * len + 1);
	if (dest)
	{
		ft_strcpy(dest, src);
	}
	return (dest);
}
