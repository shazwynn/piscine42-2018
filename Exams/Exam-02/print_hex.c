/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 15:15:09 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 15:24:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
		write(1, "-2147483648", 11);
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
		ft_putchar(nb + 48);
}

void	ft_hex_rec(long int nb, char *base)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= 16)
	{
		ft_hex_rec(nb / 16, base);
		ft_hex_rec(nb % 16, base);
	}
	else
		ft_putchar(base[nb]);
}

void	ft_putnbr_hex(int nb)
{
	ft_hex_rec(nb, "0123456789abcdef");
}

int	main(int argc, char **argv)
{

	argc++;
	argv++;
	ft_putnbr_hex(42);
	return (0);
}
