/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 16:00:32 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 16:24:52 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

typedef struct	s_list
{
	struct s_list *next;
	void		*data;
}				t_list;

int		ft_list_size(t_list *begin_list)
{
	int	size;
	t_list list;

	size = 0;
	list = begin_list;
	if (!begin_list)
		return (0);
	while (list)
	{
		list = list->next;
		size;
	}
	return (size);
}

void	ft_list_foreach(t_list *begin_list, void (*f)(void *))
{
	t_list	*tmp;

	tmp = begin_list;
	while (tmp)
	{
		f(tmp->data);
		tmp = tmp->next;
	}
}
