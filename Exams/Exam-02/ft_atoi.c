/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 13:21:55 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 13:35:22 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int	ft_atoi(const char *str)
{
	int	res;
	int i;
	int	s;

	i = 0;
	res = 0;
	s = 0;
	while (str[s] <= 32 || str[s] > 126)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[s + i] && str[s + i] >= '0' && str[s + i] <= '9')
	{
		res = (res * 10) + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

int		main(void)
{
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("12"), atoi("12"));
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("2147483647"), atoi("2147483647"));
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("-2147483648"), atoi("-2147483648"));
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("   7483648"), atoi("   7483648"));
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("-2147483648"), atoi("-2147483648"));
	printf("ft_atoi : %d\natoi : %d\n", ft_atoi("-2147483648"), atoi("-2147483648"));
	return (0);
}
