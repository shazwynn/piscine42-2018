/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 14:31:19 by algrele           #+#    #+#             */
/*   Updated: 2018/04/25 03:43:30 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <locale.h>
#include <stdlib.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	print_bits(unsigned char octet)
{
	int	n;

	n = 128;
	while (n > 0)
	{
		if (n == 8)
			ft_putstr("| ");
		if (octet >= n)
		{
			ft_putchar('1');
			ft_putchar(' ');
			octet = octet -n;
		}
		else
		{
			ft_putchar('0');
			ft_putchar(' ');
		}
		n = n / 2;
	}
	ft_putstr("| ");
}

unsigned char	reverse_bits(unsigned char octet)
{
	return (octet >> 4 | octet << 4);
}

unsigned char	inverse_bits(unsigned char octet)
{
	return (~octet);
}

void		print_bits_2(int number)
{
	void *ptr;
	char *ptr2;

	ptr = &number;
	ptr2 = (char *)ptr;
//	number = ~number;
//	printf("%d\n", number);
//	number = ~number;
//	printf("%d\n", number);
//	print_bits((unsigned char)number);
	print_bits(*ptr2);
	ptr2++;
	print_bits(*ptr2);
	ptr2++;
	print_bits(*ptr2);
	ptr2++;
	print_bits(*ptr2);
	ft_putchar('\n');
}

/*
void		print_bits_3(int number)
{
	int i = sizeof(int) * 8;;
	while (i >= 0)
	{
		if (number & (lu << i))
			ft_putchar('1');
		else
			ft_putchar('0');
		i--;
	}
	ft_putchar('\n');
}
*/

void		test_bits(int number)
{
	int i = 0;
	printf("%d\n", number);
	print_bits_2(number);
//	print_bits_2(~number);
}

void	ft_putnbr(int n);
int		ft_atoi_base(char *str, char *base);

int		*rev_bit_tab(int *tab, int i)
{
	int	j;
	int	half_len;
	int	tmp;

	j = 0;
	half_len = i / 2;
	while (j < half_len)
	{
		tmp = tab[j];
		tab[j] = tab[i - 1];
		tab[i - 1] = tmp;
		j++;
		i--;
	}
	return (tab);
}

int		main(void)
{
	setlocale(LC_ALL, "");
//	print_bits(2);
//	ft_putchar('\n');
//	printf("print bits : \n");
	print_bits(42);
	ft_putchar('\n');
//	printf("reverse bits : \n");
	print_bits(reverse_bits(42));
	ft_putchar('\n');
	print_bits(inverse_bits(42));
	ft_putchar('\n');
	test_bits(42);
//	test_bits(2147483647);
//	ft_putchar('\n');
	test_bits(-42);
//	print_bits(0);
//	ft_putchar('\n');
//	print_bits(255);
//	ft_putchar('\n');

	print_bits_2(945);
	int *tab;
	int test = 945;
	int	i = 0;
	while (test)
	{
		ft_putnbr(test & 1);
		tab[i] = test & 1;
		test = test >> 1;
		i++;
	}
	ft_putchar('\n');
	/*
	tab = rev_bit_tab(tab, i);
	int	j = 0;
	while (j < i)
	{
		ft_putnbr(tab[j]);
		ft_putchar('|');
		j++;
	}
*/	printf(" -> tient sur %d bits\n", i);
	ft_putchar('\n');

	char *octet1;
	char *octet2;

	octet1 = (char *)malloc(sizeof(char) * 9);
	octet2 = (char *)malloc(sizeof(char) * 9);
	int y = 0;
	while (y < 9)
	{
		octet1[y] = '\0';
		octet2[y++] = '\0';
	}
	int i2 = 0;
	octet1[0] = '1';
	octet1[1] = '1';
	octet1[2] = '0';
	octet1[3] = '0';
	octet2[0] = '1';
	octet2[1] = '0';
	y = 7;
	while (y > 1)
		octet2[y--] = tab[i2++] + '0';
	y = 7;
	while (y > 3)
		octet1[y--] = tab[i2++] + '0';

	int res;
	ft_putstr(octet1);
	ft_putchar('\n');
	ft_putstr(octet2);
	ft_putchar('\n');
	res = ft_atoi_base(octet1, "01");
	write(1, &res, 1);
//	ft_putnbr(ft_atoi_base(octet1, "01"));
//	ft_putchar('\n');
	res = ft_atoi_base(octet2, "01");
	write(1, &res, 1);
//	ft_putnbr(ft_atoi_base(octet2, "01"));
//	ft_putchar('\n');

	/* test 42 */
	test = 42;
	ft_putchar('\n');
	while (test)
	{
		ft_putnbr(test & 1);
		test = test >> 1;
	}
	test = 14780596;
	ft_putchar('\n');
	while (test)
	{
		ft_putnbr(test & 1);
		test = test >> 1;
	}
}
