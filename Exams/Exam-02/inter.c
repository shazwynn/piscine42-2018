/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 13:44:03 by algrele           #+#    #+#             */
/*   Updated: 2018/02/23 13:49:31 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int	main(int argc, char **argv)
{
	int	i;
	int j;

	if (argc != 3)
		return (0);
	i = 0;
	while (argv[1][i])
	{
		j = 0;
		while (argv[2][j] != argv[1][i])
			j++;
		if (argv[2][j] == argv[1][i])
			ft_putchar(argv[2][j]);
		i++;
	}

	return (0);
}
