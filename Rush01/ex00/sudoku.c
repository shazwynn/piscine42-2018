/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:14:45 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:40:42 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		main(int argc, char **argv)
{
	int		check;
	char	**copy;

	check = 0;
	if (ft_check_input(argc, argv) == 0)
	{
		ft_putstr("Error\n");
		return (0);
	}
	copy = ft_tabdup(argv);
	ft_rec(argv, 1, 0, &check);
	if (check == 1)
	{
		check = 0;
		ft_rec_rev(copy, 1, 0, &check);
		if (ft_compare_res(argv, copy) == 1)
			ft_display(argv);
		else
			ft_putstr("Error\n");
	}
	else
		ft_putstr("Error\n");
	ft_free_tab(copy);
	return (0);
}
