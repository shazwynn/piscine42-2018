/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtrack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:14:31 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:00:18 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

void	ft_check_finish(int x, int *check)
{
	if (x == 10 && *check != 1)
		*check = *check + 1;
}

void	ft_rec(char **grid, int x, int y, int *check)
{
	char i;

	i = '1';
	ft_check_finish(x, check);
	if (x != 10 && *check != 1)
	{
		if (grid[x][y] == '.')
		{
			while (i <= '9' && *check == 0)
			{
				if (ft_check_main(grid, x, y, i) == 1)
				{
					grid[x][y] = i;
					if (x == 9 && y == 8)
						*check = *check + 1;
					ft_rec(grid, next_x(x, y), next_y(y), check);
				}
				i++;
			}
			if (*check != 1)
				grid[x][y] = '.';
		}
		else
			ft_rec(grid, next_x(x, y), next_y(y), check);
	}
}

void	ft_rec_rev(char **grid, int x, int y, int *check)
{
	char i;

	i = '9';
	ft_check_finish(x, check);
	if (x != 10 && *check != 1)
	{
		if (grid[x][y] == '.')
		{
			while (i >= '1' && *check == 0)
			{
				if (ft_check_main(grid, x, y, i) == 1)
				{
					grid[x][y] = i;
					if (x == 9 && y == 8)
						*check = *check + 1;
					ft_rec(grid, next_x(x, y), next_y(y), check);
				}
				i--;
			}
			if (*check != 1)
				grid[x][y] = '.';
		}
		else
			ft_rec(grid, next_x(x, y), next_y(y), check);
	}
}

int		ft_compare_res(char **argv, char **copy)
{
	int i;
	int j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] != copy[i][j])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}
