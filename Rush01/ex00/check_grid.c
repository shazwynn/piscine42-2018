/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:15:00 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:40:41 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int	check_line(char *str, char value)
{
	int i;

	i = 0;
	while (str[i] != '\0' && str[i] != value)
	{
		i++;
	}
	if (str[i] == value)
		return (0);
	else
		return (1);
}

int	check_column(char **str, int position, char value)
{
	int i;

	i = 1;
	while (i != 9 && str[i][position] != value)
		i++;
	if (str[i][position] == value)
		return (0);
	else
		return (1);
}

int	check_square(char **str, int pos_x, int pos_y, char value)
{
	int i;
	int j;

	i = 1;
	pos_x = pos_x - 1;
	pos_x = pos_x - pos_x % 3;
	pos_y = pos_y - pos_y % 3;
	while (i < 4)
	{
		j = 0;
		while (j < 3)
		{
			if (value == str[pos_x + i][pos_y + j])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	ft_check_input_grid(char **grid)
{
	int	i;
	int	j;

	i = 1;
	while (grid[i])
	{
		j = 0;
		while (grid[i][j])
		{
			if (grid[i][j] != '.' && ft_check_main(grid, i, j, grid[i][j]) == 0)
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	ft_check_main(char **grid, int x, int y, char c)
{
	if (check_line(grid[x], c) == 0 || check_column(grid, y, c) == 0
			|| check_square(grid, x, y, c) == 0)
		return (0);
	else
		return (1);
}
