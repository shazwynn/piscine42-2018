/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:14:54 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:34:51 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		ft_check_input(int argc, char **argv)
{
	int i;
	int j;

	i = 1;
	if (argc != 10)
		return (0);
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			if ((argv[i][j] < '1' || argv[i][j] > '9') && argv[i][j] != '.')
				return (0);
			j++;
		}
		if (j != 9)
			return (0);
		i++;
	}
	if (ft_check_input_grid(argv) == 0)
		return (0);
	return (1);
}

char	*ft_strcpy(char *dst, char *src)
{
	int	i;

	i = 0;
	while (src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

char	**ft_tabdup(char **tab)
{
	char	**copy;
	int		i;

	copy = (char **)malloc(sizeof(char*) * 11);
	if (copy)
	{
		i = 0;
		while (i < 10)
		{
			copy[i] = (char*)malloc(sizeof(char) * (10));
			if (copy[i])
				copy[i] = ft_strcpy(copy[i], tab[i]);
			i++;
		}
		copy[i] = 0;
	}
	return (copy);
}

void	ft_free_tab(char **tab)
{
	int	i;

	i = 0;
	while (tab && tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}
