/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:14:38 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:39:08 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUDOKU_H
# define SUDOKU_H

# include <unistd.h>
# include <stdlib.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_display(char **tab);
int		check_line(char *str, char value);
int		check_column(char **str, int position, char value);
int		check_grid(char **str, int pos_x, int pos_y, char value);
int		ft_check_input(int argc, char **argv);
int		ft_check_main(char **grid, int x, int y, char c);
int		ft_check_input_grid(char **grid);
void	ft_rec(char **grid, int x, int y, int *check);
void	ft_rec_rev(char **grid, int x, int y, int *check);
char	**ft_tabdup(char **tab);
void	ft_free_tab(char **tab);
int		ft_compare_res(char **argv, char **copy);
int		next_x(int x, int y);
int		next_y(int y);

#endif
