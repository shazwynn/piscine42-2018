/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 19:15:05 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 23:17:10 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_display(char **tab)
{
	int	i;
	int j;

	i = 1;
	while (i < 10)
	{
		j = 0;
		while (j < 9)
		{
			ft_putchar(tab[i][j]);
			ft_putchar(' ');
			j++;
		}
		i++;
		ft_putchar('\n');
	}
}

void	put_line(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		if (*str + 1 != '\0')
			ft_putchar(' ');
		if (i % 3 == 2)
			ft_putchar('|');
		i++;
	}
	ft_putchar('\n');
}

void	ft_display_better(char **tab)
{
	int i;
	int j;

	i = 1;
	ft_putstr("\n");
	while (i < 10)
	{
		if (i == 4 || i == 7)
		{
			j = 0;
			while (j < 21)
			{
				ft_putchar('-');
				j++;
			}
			ft_putchar('\n');
		}
		put_line(tab[i]);
		i++;
	}
}
