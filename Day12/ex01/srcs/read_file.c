/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 03:45:12 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 10:11:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cat.h"

int				ft_display_file(char *file)
{
	int				fd;
	unsigned long	size;

	fd = open(file, O_DIRECTORY);
	if (fd != -1)
	{
		ft_putstr("cat: ");
		ft_putstr(file);
		ft_putstr(": Is a directory\n");
		return (0);
	}
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("cat: ");
		ft_putstr(file);
		ft_putstr(": No such file or directory\n");
		return (0);
	}
	size = count_size(fd);
	close(fd);
	fd = open(file, O_RDONLY);
	read_input(fd, size, 0, 0);
	close(fd);
	return (0);
}

int				ft_display_stdin(void)
{
	read_input(0, MAX_SIZE, 0, 0);
	return (0);
}

int				read_input(int fd, unsigned long size, unsigned long i, int j)
{
	char			str[size + 1];
	int				l;
	int				ret;
	char			buf[BUF_SIZE];

	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		j = 0;
		while (j < ret)
		{
			str[i++] = buf[j];
			if (buf[j++] == '\n')
			{
				l = 0;
				str[i] = '\0';
				ft_putstr(str);
				while (str[l])
					str[l++] = '\0';
				i = 0;
			}
		}
	}
	str[i] = '\0';
	ft_putstr(str);
	return (0);
}

unsigned long	count_size(int fd)
{
	char			buf[BUF_SIZE];
	int				ret;
	unsigned long	total;

	total = 0;
	while ((ret = read(fd, buf, BUF_SIZE)))
		total = total + ret;
	return (total);
}
