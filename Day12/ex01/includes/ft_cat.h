/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cat.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 02:47:56 by algrele           #+#    #+#             */
/*   Updated: 2018/02/27 02:59:37 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CAT_H
# define FT_CAT_H

# include <unistd.h>
# include <fcntl.h>
# include <errno.h>

# define BUF_SIZE 100
# define MAX_SIZE 7800

/*
** DISPLAY
*/

void			ft_putchar(char c);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr(char *str);
void			ft_putstr_fd(char *str, int fd);
void			ft_putnbr(int nb);

/*
** READ_FILE
*/

int				ft_display_file(char *file);
int				ft_display_stdin(void);
unsigned long	count_size(int fd);
int				read_input(int fd, unsigned long size, unsigned long i, int j);

#endif
