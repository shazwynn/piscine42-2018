/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 03:45:12 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 12:11:47 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_tail.h"

void			ft_print_name(char *file)
{
	ft_putstr("==> ");
	ft_putstr(file);
	ft_putstr(" <==\n");
}

int				ft_display_file(char *file, int size, int print)
{
	int				fd;
	unsigned long	file_size;

	fd = open(file, O_DIRECTORY);
	if (fd != -1)
		return (0);
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("tail: ");
		ft_putstr(file);
		ft_putstr(": No such file or directory\n");
		return (0);
	}
	if (print == 1)
		ft_print_name(file);
	file_size = count_size(fd);
	close(fd);
	fd = open(file, O_RDONLY);
	read_input(fd, file_size, size);
	close(fd);
	return (0);
}

int				read_input(int fd, unsigned long size, int s)
{
	char			str[size + 1];
	int				ret;
	char			buf[BUF_SIZE];
	int				j;
	unsigned long	i;

	j = 0;
	i = 0;
	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		j = 0;
		while (j < ret)
		{
			str[i++] = buf[j];
			j++;
		}
	}
	str[i] = '\0';
	ft_putstr_start(str, size, s);
	return (0);
}

unsigned long	count_size(int fd)
{
	char			buf[BUF_SIZE];
	int				ret;
	unsigned long	total;

	total = 0;
	while ((ret = read(fd, buf, BUF_SIZE)))
		total = total + ret;
	return (total);
}
