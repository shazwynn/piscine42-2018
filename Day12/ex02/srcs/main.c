/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 01:52:50 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 12:09:00 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_tail.h"

int		put_missing_args(void)
{
	ft_putstr("tail: option requires an argument -- c\n");
	ft_putstr("usage: tail [-F | -f | -r] [-q] [-b # | ");
	ft_putstr("-c # | -n #] [file ...]\n");
	return (0);
}

int		main(int argc, char **argv)
{
	int	i;

	if (argc == 1)
		return (0);
	i = 3;
	if (argc > 1)
	{
		if (ft_strcmp(argv[1], "-c") != 0)
			return (0);
		if (argc == 2)
			return (put_missing_args());
		while (i < argc)
		{
			if (argc == 4)
				ft_display_file(argv[i], ft_atoi(argv[2]), 0);
			if (argc > 4)
			{
				ft_display_file(argv[i], ft_atoi(argv[2]), 1);
				if (i != argc - 1)
					ft_putchar('\n');
			}
			i++;
		}
	}
	return (0);
}
