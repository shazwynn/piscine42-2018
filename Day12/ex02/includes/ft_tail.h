/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 10:04:48 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 11:53:06 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TAIL_H
# define FT_TAIL_H

# include <unistd.h>
# include <fcntl.h>
# include <errno.h>
# include <stdlib.h>

# define BUF_SIZE 100
# define MAX_SIZE 7800

/*
** DISPLAY
*/

void			ft_putchar(char c);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr(char *str);
void			ft_putstr_fd(char *str, int fd);
void			ft_putstr_start(char *str, int size, int start);

/*
** READ_FILE
*/

int				ft_display_file(char *file, int size, int file_num);
unsigned long	count_size(int fd);
int				read_input(int fd, unsigned long size, int s);

/*
** TOOLS
*/

int				ft_strcmp(char *s1, char *s2);
int				ft_atoi(char *str);

#endif
