/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 10:00:40 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 12:57:26 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int	i;
	int croissant;
	int pain_au_chocolat;

	if (length < 2)
		return (1);
	if (!tab)
		return (1);
	croissant = 0;
	pain_au_chocolat = 0;
	i = 0;
	while (i < length - 1)
	{
		if (f(tab[i + 1], tab[i]) < 0)
			pain_au_chocolat++;
		else if (f(tab[i + 1], tab[i]) > 0)
			croissant++;
		i++;
	}
	if (pain_au_chocolat != 0 && croissant != 0)
		return (0);
	return (1);
}
