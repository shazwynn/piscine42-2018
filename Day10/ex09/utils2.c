/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 15:22:10 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 15:07:51 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "adoop.h"

int	ft_mod(int a, int b)
{
	return (a % b);
}

int	ft_calcul(int a, int b, int (*f)(int, int))
{
	return (f(a, b));
}

int	ft_len(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int	ft_cmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while ((s1[i] && s2[i]) && s1[i] == s2[i])
		i++;
	return (s1[i] - s2[i]);
}
