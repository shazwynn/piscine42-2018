/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adoop.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 00:56:49 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 15:11:33 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "adoop.h"
#include "ft_opp.h"

int	ft_usage(int a, int b)
{
	ft_putstr("error : only [ ");
	ft_putstr(g_opptab[0].op);
	ft_putstr(" ");
	ft_putstr(g_opptab[1].op);
	ft_putstr(" ");
	ft_putstr(g_opptab[2].op);
	ft_putstr(" ");
	ft_putstr(g_opptab[3].op);
	ft_putstr(" ");
	ft_putstr(g_opptab[4].op);
	ft_putstr(" ");
	ft_putstr("] are accepted.\n");
	return (0);
}

int	main(int argc, char **argv)
{
	int	a;
	int b;
	int	i;

	if (argc != 4)
		return (0);
	a = ft_atoi(argv[1]);
	b = ft_atoi(argv[3]);
	i = 0;
	while (i < 6)
	{
		if (i == 5)
			return (ft_usage(0, 0));
		else if (ft_cmp(argv[2], g_opptab[i].op) == 0)
		{
			if (i == 3 && b == 0)
				return (ft_putstr("Stop : division by zero\n"));
			if (i == 4 && b == 0)
				return (ft_putstr)("Stop : modulo by zero\n");
			else
				return (ft_putnbr(ft_calcul(a, b, g_opptab[i].f)));
		}
		i++;
	}
	return (0);
}
