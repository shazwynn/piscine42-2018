/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 11:49:44 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 15:09:43 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ADOOP_H
# define ADOOP_H

# include <stdlib.h>
# include <unistd.h>

typedef struct	s_opp
{
	char		*op;
	int			(*f)(int, int);
}				t_opp;

/*
** UTILS
*/

int				ft_atoi(char *str);
int				ft_add(int a, int b);
int				ft_sub(int a, int b);
int				ft_div(int a, int b);
int				ft_mul(int a, int b);

/*
** UTILS2
*/

int				ft_mod(int a, int b);
int				ft_calcul(int a, int b, int (*f)(int, int));
int				ft_len(char *str);
int				ft_cmp(char *s1, char *s2);

/*
** DISPLAY
*/

void			ft_putchar(char c);
int				ft_putstr(char *str);
int				ft_putnbr(int nb);

/*
** ADOOP
*/

int				ft_usage(int a, int b);

#endif
