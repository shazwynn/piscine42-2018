/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 11:49:31 by algrele           #+#    #+#             */
/*   Updated: 2018/02/21 11:49:59 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

int	main(int argc, char **argv)
{
	int	a;
	int b;
	int	operateur;
	int (*op[5])(int, int);

	if (argc != 4)
		return (0);
	a = ft_atoi(argv[1]);
	b = ft_atoi(argv[3]);
	op[0] = &ft_add;
	op[1] = &ft_sub;
	op[2] = &ft_div;
	op[3] = &ft_mod;
	op[4] = &ft_mul;
	operateur = test_expression(argv[2]);
	if (operateur == -1)
		ft_putchar('0');
	else if (operateur == 4 && b == 0)
		ft_putstr("Stop : modulo by zero");
	else if (operateur == 3 && b == 0)
		ft_putstr("Stop : division by zero");
	else
		ft_putnbr(op[operateur - 1](a, b));
	ft_putchar('\n');
	return (0);
}
