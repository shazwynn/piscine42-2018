/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 11:49:44 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 13:05:25 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DOOP_H
# define DOOP_H

# include <stdlib.h>
# include <unistd.h>

/*
** UTILS
*/

int		ft_atoi(char *str);
int		ft_add(int a, int b);
int		ft_sub(int a, int b);
int		ft_div(int a, int b);
int		ft_mul(int a, int b);

/*
** UTILS2
*/

int		ft_mod(int a, int b);
int		ft_calcul(int a, int b, int (*f)(int, int));
int		ft_strlen(char *str);
int		test_expression(char *str);

/*
** DISPLAY
*/

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nb);

#endif
