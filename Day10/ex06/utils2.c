/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 15:22:10 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 13:05:09 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

int	ft_mod(int a, int b)
{
	return (a % b);
}

int	ft_calcul(int a, int b, int (*f)(int, int))
{
	return (f(a, b));
}

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int	test_expression(char *expression)
{
	int	i;

	i = 0;
	if (ft_strlen(expression) != 1)
		return (-1);
	if (expression[i] == '+')
		return (1);
	if (expression[i] == '-')
		return (2);
	if (expression[i] == '/')
		return (3);
	if (expression[i] == '%')
		return (4);
	if (expression[i] == '*')
		return (5);
	else
		return (-1);
}
