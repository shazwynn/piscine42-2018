/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 15:20:39 by algrele           #+#    #+#             */
/*   Updated: 2018/02/21 11:50:05 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "doop.h"

int	ft_atoi(char *str)
{
	int	res;
	int i;
	int s;

	i = 0;
	res = 0;
	s = 0;
	while (str[s] == ' ' || str[s] <= 31 || str[s] >= 127)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] >= '0' && str[i + s] <= '9')
	{
		res = res * 10 + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

int	ft_add(int a, int b)
{
	return (a + b);
}

int	ft_sub(int a, int b)
{
	return (a - b);
}

int	ft_div(int a, int b)
{
	return (a / b);
}

int	ft_mul(int a, int b)
{
	return (a * b);
}
