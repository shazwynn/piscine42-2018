/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_wordtab.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 13:02:08 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 14:52:45 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap_str(char **s1, char **s2)
{
	char *tmp;

	tmp = *s1;
	*s1 = *s2;
	*s2 = tmp;
}

void	ft_advanced_sort_wordtab(char **tab, int (*cmp)(char *, char *))
{
	int	i;
	int d;

	i = 0;
	d = 1;
	if (tab && cmp)
	{
		while (d > 0)
		{
			d = 0;
			i = 0;
			while (tab[i] && tab[i + 1])
			{
				if (cmp(tab[i], tab[i + 1]) > 0)
				{
					ft_swap_str(&tab[i], &tab[i + 1]);
					d++;
				}
				i++;
			}
		}
	}
}
