/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_wordtab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 12:04:58 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 14:46:57 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	return (s1[i] - s2[i]);
}

void	ft_swap_str(char **s1, char **s2)
{
	char *tmp;

	tmp = *s1;
	*s1 = *s2;
	*s2 = tmp;
}

void	ft_sort_wordtab(char **tab)
{
	int	i;
	int d;

	i = 0;
	d = 1;
	if (tab)
	{
		while (d > 0)
		{
			d = 0;
			i = 0;
			while (tab[i] && tab[i + 1])
			{
				if (ft_strcmp(tab[i], tab[i + 1]) > 0)
				{
					ft_swap_str(&tab[i], &tab[i + 1]);
					d++;
				}
				i++;
			}
		}
	}
}
