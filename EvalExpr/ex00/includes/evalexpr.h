/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   evalexpr.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 18:42:13 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 23:43:22 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVALEXPR_H
# define EVALEXPR_H

# include <unistd.h>
# include <stdlib.h>

# define OPERATORS "%*/+-()R"
# define PRIORITY_1 "%*/"
# define PRIORITY_2 "+-"
# define PRIORITY_3 "("
# define NUMBERS "0123456789"
# define RESET_COLOR "\033[0m"
# define P_RED "\e[38;5;13m"
# define P_BLUE "\e[38;5;39m"
# define YELLOW "\e[38;5;226m"
# define SUN "\e[38;5;208m"

typedef struct s_list
{
	int				nb;
	char			op;
	struct s_list	*next;
}				t_list;

/*
** DISPLAY
*/

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nb);
void	ft_print_list(t_list *list);

/*
** STR_TOOLS
*/

char	*ft_strsub(char *str, int start, int size);
char	*ft_strdup(char *s1);
char	*ft_strncpy(char *dst, char *src, int n);
int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);

/*
** STACK_TOOLS
*/

void	stack_clear(char **stack, int operators);
char	*stack_clear_par(char *stack);
int		is_prio(char op, char stack_top);

/*
** CALCUL
*/

int		ft_atoi(char *str);
int		ft_doop(int a, int b, char op);

/*
** CALCUL2
*/

int		ft_is_in(char c, char *charset);
int		find_next_calcul(t_list **begin_list, int operators, int res);
int		ft_calcul_postfix(t_list **begin_list, int operators);
int		calcul(char *str);
t_list	*to_infix(char *str, int *operators, int i);

/*
** EVAL_EXPR
*/

int		eval_expr(char *str);
t_list	*create_post(t_list **begin_list, int operators);
t_list	*to_postfix(t_list **begin_list, char *stack);

/*
** LST_TOOLS
*/

t_list	*list_create_nb(int num);
t_list	*list_create_op(char op);
int		*ft_list_push_back_op(t_list **begin_list, char op, int *opn, int c);
int		ft_list_push_back_nb(t_list **begin_list, int nb);

/*
** LST_TOOLS2
*/

void	list_delete_elems(t_list *elem, t_list *elem2);
void	list_delete_elem(t_list *elem);
char	*lst_dump_stack(t_list **post_fix_begin, t_list *cur, char *stack);
char	*end_dump_stack(t_list **post_fix_begin, char *stack);

#endif
