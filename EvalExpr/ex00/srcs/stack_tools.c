/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 16:13:07 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 20:53:00 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

void	stack_clear(char **stack, int operators)
{
	int	i;

	i = 0;
	while (i < operators)
	{
		stack[0][i] = '\0';
		i++;
	}
}

char	*stack_clear_par(char *stack)
{
	int	len;

	len = ft_strlen(stack);
	while (stack[len - 1] && stack[len - 1] != '(')
	{
		stack[len - 1] = '\0';
		len--;
	}
	if (stack[len - 1] == '(')
		stack[len - 1] = '\0';
	return (stack);
}

int		is_prio(char op, char stack_top)
{
	int	op_int;
	int	stack_top_int;

	op_int = 0;
	stack_top_int = 0;
	if (op == '(')
		op_int = 3;
	if (stack_top == '(')
		stack_top_int = 3;
	if (op == '+' || op == '-')
		op_int = 2;
	if (stack_top == '+' || stack_top == '-')
		stack_top_int = 2;
	if (op == '%' || op == '/' || op == '*')
		op_int = 1;
	if (stack_top == '%' || stack_top == '/' || stack_top == '*')
		stack_top_int = 1;
	if (stack_top_int >= op_int)
		return (1);
	else
		return (0);
}
