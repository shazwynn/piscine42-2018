/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 19:28:06 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 20:52:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

char	*ft_strsub(char *str, int start, int size)
{
	char	*sub;

	sub = NULL;
	if (!str)
		return (NULL);
	sub = (char *)malloc(sizeof(char) * (size + 1));
	if (!sub)
		return (NULL);
	ft_strncpy(sub, str + start, size);
	sub[size] = '\0';
	return (sub);
}

char	*ft_strdup(char *s1)
{
	char	*dst;
	int		i;

	dst = (char*)malloc(sizeof(char) * (ft_strlen(s1) + 1));
	if (dst)
	{
		i = 0;
		while (s1[i])
		{
			dst[i] = s1[i];
			i++;
		}
		dst[i] = '\0';
	}
	return (dst);
}

char	*ft_strncpy(char *dst, char *src, int n)
{
	int i;

	i = 0;
	while (src[i] != '\0' && i < n)
	{
		dst[i] = src[i];
		i++;
	}
	while (i < n)
	{
		dst[i] = '\0';
		i++;
	}
	return (dst);
}

int		ft_strlen(char *str)
{
	int len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

int		ft_cmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while ((s1[i] && s2[i]) && s1[i] == s2[i])
		i++;
	return (s1[i] - s2[i]);
}
