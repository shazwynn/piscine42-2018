/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calcul2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 19:48:49 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 23:38:30 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

int		ft_is_in(char c, char *charset)
{
	int	i;

	i = 0;
	while (charset[i])
	{
		if (c == charset[i])
			return (1);
		i++;
	}
	return (0);
}

int		find_next_calcul(t_list **begin_list, int operators, int res)
{
	t_list	*current;
	t_list	*tmp;
	t_list	*tmp2;

	current = *begin_list;
	current = current->next;
	if (operators > 0)
	{
		while (!current->op)
		{
			if (current->next->next->op)
			{
				tmp = current->next;
				tmp2 = current->next->next;
				res = ft_doop(current->nb, tmp->nb, tmp2->op);
				current->nb = res;
				current->next = current->next->next->next;
				list_delete_elems(tmp, tmp2);
				return (res);
			}
			else
				current = current->next;
		}
	}
	return (res);
}

int		ft_calcul_postfix(t_list **begin_list, int operators)
{
	int		res;
	t_list	*tmp;

	tmp = *begin_list;
	res = 0;
	while (operators > 0)
	{
		find_next_calcul(begin_list, operators, 0);
		operators--;
	}
	tmp = tmp->next;
	res = tmp->nb;
	return (res);
}

int		calcul(char *str)
{
	int		res;
	int		operators;
	t_list	*list;

	operators = 0;
	res = 0;
	list = to_infix(str, &operators, 0);
	list = create_post(&list, operators);
	res = ft_calcul_postfix(&list, operators);
	return (res);
}

t_list	*to_infix(char *str, int *operators, int i)
{
	int		k;
	t_list	*list;

	list = list_create_op('R');
	while (str[i])
	{
		k = 0;
		if (str[i] == '-' && (!ft_is_in(str[i + 1], NUMBERS)))
			operators = ft_list_push_back_op(&list, str[i], operators, 1);
		else if (ft_is_in(str[i], OPERATORS) && str[i] != '-')
			ft_list_push_back_op(&list, str[i], operators, 1);
		else if (str[i] == '-' && str[i + 1] && ft_is_in(str[i + 1], NUMBERS))
			k++;
		while (ft_is_in(str[i + k], NUMBERS))
			k++;
		if (k && ft_list_push_back_nb(&list, ft_atoi(ft_strsub(str, i, k))))
			i = i + k;
		else
			i++;
	}
	return (list);
}
