/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 15:19:24 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 05:55:20 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	int		i;
	char	*smi;

	i = 0;
	smi = "-2147483648";
	if (nb == -2147483648)
		while (i < 11)
			ft_putchar(smi[i++]);
	else
	{
		if (nb < 0)
		{
			ft_putchar('-');
			nb = -nb;
		}
		if (nb >= 10)
		{
			ft_putnbr(nb / 10);
			ft_putnbr(nb % 10);
		}
		else
			ft_putchar(nb + '0');
	}
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			ft_putchar(str[i]);
			i++;
		}
	}
}

void	ft_print_list(t_list *list)
{
	while (list)
	{
		ft_putstr("[ ");
		if (list->op)
		{
			ft_putstr(P_RED);
			ft_putchar(list->op);
			ft_putstr(RESET_COLOR);
		}
		else if (list->nb)
		{
			ft_putstr(P_BLUE);
			ft_putnbr(list->nb);
			ft_putstr(RESET_COLOR);
		}
		else
			ft_putstr("null");
		ft_putstr(" ] ");
		ft_putstr("-> ");
		list = list->next;
	}
	ft_putstr("NULL\n");
}
