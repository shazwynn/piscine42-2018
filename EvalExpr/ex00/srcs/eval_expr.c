/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval_expr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 19:20:35 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 23:39:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

t_list	*create_post(t_list **begin_list, int operators)
{
	char	*stack;
	t_list	*current;
	t_list	*post_fix_begin;

	current = *begin_list;
	if (!(stack = (char *)malloc(sizeof(char) * (operators + 1))))
		return (0);
	stack_clear(&stack, operators);
	post_fix_begin = to_postfix(begin_list, stack);
	return (post_fix_begin);
}

t_list	*to_postfix(t_list **begin_list, char *stack)
{
	t_list	*current;
	t_list	*post;
	int		i;

	current = *begin_list;
	if (current->op == 'R')
		post = list_create_op('R');
	current = current->next;
	while (current)
	{
		i = 0;
		if (!current->op)
			ft_list_push_back_nb(&post, current->nb);
		else if (current->op == '(')
			stack[ft_strlen(stack)] = '(';
		else if (current->op == ')')
			stack = lst_dump_stack(&post, current, stack);
		else if (current->op && !stack[0])
			stack[i] = current->op;
		else if (current->op && stack[0])
		{
			while (stack[i])
				i++;
			if (is_prio(current->op, stack[i - 1]))
				stack[i] = current->op;
			else
				stack = lst_dump_stack(&post, current, stack);
		}
		current = current->next;
	}
	stack = end_dump_stack(&post, stack);
	return (post);
}

int		eval_expr(char *str)
{
	int		result;
	char	*expr;

	result = 0;
	if (str)
	{
		expr = ft_strdup(str);
		if (!expr)
			return (0);
		result = calcul(expr);
		free(expr);
	}
	return (result);
}
