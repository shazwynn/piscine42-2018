/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 02:53:32 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 23:12:04 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

t_list	*list_create_nb(int num)
{
	t_list	*first;

	first = (t_list*)malloc(sizeof(t_list));
	if (first)
	{
		first->nb = num;
		first->next = NULL;
	}
	return (first);
}

t_list	*list_create_op(char op)
{
	t_list	*first;

	first = (t_list*)malloc(sizeof(t_list));
	if (first)
	{
		first->op = op;
		first->next = NULL;
	}
	return (first);
}

int		*ft_list_push_back_op(t_list **begin_list, char op, int *opn, int c)
{
	t_list	*new;
	t_list	*findlast;

	new = (t_list*)malloc(sizeof(t_list));
	if (new)
	{
		new->op = op;
		new->next = NULL;
	}
	findlast = *begin_list;
	while (findlast->next)
		findlast = findlast->next;
	findlast->next = new;
	if (op != ')' && op != '(' && c == 1)
		*opn = *opn + 1;
	return (opn);
}

int		ft_list_push_back_nb(t_list **begin_list, int nb)
{
	t_list	*new;
	t_list	*findlast;

	new = (t_list*)malloc(sizeof(t_list));
	if (new)
	{
		new->nb = nb;
		new->next = NULL;
	}
	findlast = *begin_list;
	while (findlast->next)
		findlast = findlast->next;
	findlast->next = new;
	return (1);
}
