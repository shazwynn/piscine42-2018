/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_tool2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 19:11:33 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 23:10:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalexpr.h"

void	list_delete_elems(t_list *elem, t_list *elem2)
{
	free(elem);
	free(elem2);
}

void	list_delete_elem(t_list *elem)
{
	free(elem);
}

char	*lst_dump_stack(t_list **post_fix_begin, t_list *cur, char *stack)
{
	int	i;

	i = ft_strlen(stack);
	while (i > 0)
	{
		if (stack[i - 1] != '(')
			ft_list_push_back_op(post_fix_begin, stack[i - 1], 0, 0);
		i--;
	}
	stack = stack_clear_par(stack);
	if (cur->op != ')')
		stack[i] = cur->op;
	return (stack);
}

char	*end_dump_stack(t_list **post_fix_begin, char *stack)
{
	int	i;

	i = ft_strlen(stack);
	while (i > 0)
	{
		if (stack[i - 1] != '(')
			ft_list_push_back_op(post_fix_begin, stack[i - 1], 0, 0);
		i--;
	}
	stack_clear(&stack, ft_strlen(stack));
	return (stack);
}
