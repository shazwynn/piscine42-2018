/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 20:14:34 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 21:18:19 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_clear(t_list **begin_list)
{
	t_list	*findlast;
	t_list	*tmp;

	if (!begin_list)
		return ;
	findlast = *begin_list;
	while (findlast)
	{
		tmp = findlast->next;
		findlast->next = NULL;
		findlast->data = NULL;
		free(findlast);
		findlast = tmp;
	}
	*begin_list = NULL;
}
