/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_find.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 00:28:17 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 17:30:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_find(t_list *begin_list, void *data_ref, int (*cmp)())
{
	t_list	*findme;

	if (begin_list)
	{
		findme = begin_list;
		while (findme && cmp)
		{
			if (cmp(findme->data, data_ref) == 0)
				return (findme);
			findme = findme->next;
		}
	}
	return (NULL);
}
