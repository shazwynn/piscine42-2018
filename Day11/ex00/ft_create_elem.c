/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_elem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 01:50:03 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 02:05:01 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_create_elem(void *data)
{
	t_list	*first;

	first = (t_list*)malloc(sizeof(t_list));
	if (first)
	{
		first->data = data;
		first->next = NULL;
	}
	return (first);
}
