/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 02:37:47 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 14:36:49 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	list_push_front(t_list **begin_list, void *data)
{
	t_list	*first;

	first = ft_create_elem(data);
	if (first && !(*begin_list))
	{
		*begin_list = first;
	}
	else if (first)
	{
		first->data = data;
		first->next = *begin_list;
		*begin_list = first;
	}
}

t_list	*ft_list_push_params(int ac, char **av)
{
	t_list	*first;
	t_list	*new;
	int		i;

	first = NULL;
	if (ac > 1)
	{
		if (!(first = ft_create_elem(av[1])))
			return (first);
		i = 2;
		new = first;
		while (i < ac)
		{
			list_push_front(&first, av[i]);
			i++;
		}
	}
	return (first);
}
