/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 02:05:31 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 15:02:02 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *new;
	t_list *list;

	if (!begin_list)
		return ;
	if (*begin_list)
	{
		list = *begin_list;
		while (list->next)
			list = list->next;
		new = ft_create_elem(data);
		list->next = new;
	}
	else
	{
		new = ft_create_elem(data);
		*begin_list = new;
	}
}
