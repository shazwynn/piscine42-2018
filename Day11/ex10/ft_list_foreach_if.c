/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach_if.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 23:43:29 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 17:28:42 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_foreach_if(t_list *begin_list, void (*f)(void *),
		void *data_ref, int (*cmp)())
{
	t_list	*tmp;

	if (begin_list)
	{
		tmp = begin_list;
		while (tmp && f && cmp)
		{
			if (cmp(tmp->data, data_ref) == 0)
				f(tmp->data);
			tmp = tmp->next;
		}
	}
}
