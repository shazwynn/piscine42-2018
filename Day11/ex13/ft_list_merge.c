/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 01:26:47 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 13:29:57 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_merge(t_list **begin_list1, t_list *begin_list2)
{
	t_list *find_end1;

	if (!*begin_list1 || !begin_list1)
	{
		*begin_list1 = *begin_list2;
		return ;
	}
	find_end1 = *begin_list1;
	while (find_end1->next)
		find_end1 = find_end1->next;
	find_end1->next = begin_list2;
}
