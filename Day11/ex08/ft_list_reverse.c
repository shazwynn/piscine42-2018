/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 22:36:19 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 17:22:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

int		ft_list_size(t_list *begin_list)
{
	t_list	*tmp;
	int		count;

	count = 0;
	tmp = begin_list;
	while (tmp)
	{
		tmp = tmp->next;
		count++;
	}
	return (count);
}

void	ft_list_reverse(t_list **begin_list)
{
	t_list	*current;
	t_list	*previous;
	t_list	*next;

	previous = NULL;
	if (begin_list && *begin_list)
	{
		current = *begin_list;
		while (current)
		{
			next = current->next;
			current->next = previous;
			previous = current;
			current = next;
		}
		*begin_list = previous;
	}
}
