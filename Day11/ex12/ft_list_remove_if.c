/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 00:36:19 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 17:33:23 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_delete_elem(t_list **elem)
{
	t_list *delete;

	delete = *elem;
	free(delete);
}

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list	*prev;
	t_list	*tmp;

	tmp = NULL;
	if (!begin_list || !*begin_list || !cmp)
		return ;
	prev = *begin_list;
	while (prev->next)
	{
		if (cmp(prev->next->data, data_ref) == 0)
		{
			tmp = prev->next->next;
			ft_list_delete_elem(&prev->next);
			prev->next = tmp;
		}
		else
			prev = prev->next;
	}
	prev = *begin_list;
	if (cmp(prev->data, data_ref) == 0)
	{
		*begin_list = prev->next;
		ft_list_delete_elem(&prev);
	}
}
