/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 21:19:40 by algrele           #+#    #+#             */
/*   Updated: 2018/03/01 14:55:33 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list		*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	unsigned int		i;

	i = nbr;
	while (i-- && begin_list != '\0')
		begin_list = begin_list->next;
	if (begin_list == NULL)
		return (NULL);
	else
		return (begin_list);
}
