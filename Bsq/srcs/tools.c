/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 22:26:09 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 22:29:42 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	free_utils(t_deathstar *bsq, int **tab, t_grid *grid)
{
	if (grid->grid)
		free(grid->grid);
	free(grid);
	free(bsq);
	if (tab[0])
		free(tab[0]);
	if (tab[1])
		free(tab[1]);
	free(tab);
}
