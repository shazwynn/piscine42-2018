/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 14:37:52 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 21:49:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int			switch_lines(int **tab, int *cur, int *i)
{
	int	size;

	size = *i;
	if (*cur == 1)
		swap_tab(tab);
	else if (*cur < 1)
		*cur = *cur + 1;
	*i = -1;
	return (size);
}

/*
** find_bsq returns 0 after finding bsq
*/

void		find_bsq(t_grid *grid, t_deathstar **bsq, int **tab, char *tmp)
{
	int			i;
	int			j;
	int			cur;

	i = -1;
	j = 0;
	cur = 0;
	while (tmp[++i])
	{
		if (tmp[i] == '\n')
		{
			tmp = tmp + switch_lines(tab, &cur, &i) + 1;
			j++;
		}
		else if (tmp[i] == grid->obstacle)
			tab[cur][i] = 0;
		else
		{
			tab[cur][i] = find_min(tab, cur, i) + 1;
			if (!bsq[0])
				bsq[0] = create_bsq(bsq[0], i, j, tab[cur][i]);
			else if (tab[cur][i] > bsq[0]->size)
				bsq[0] = create_bsq(bsq[0], i, j, tab[cur][i]);
		}
	}
}

/*
** create_bsq mallocs a struct for bsq and initialises values.
*/

t_deathstar	*create_bsq(t_deathstar *bsq, int x, int y, int size)
{
	if (!bsq)
		bsq = (t_deathstar*)malloc(sizeof(t_deathstar));
	if (!bsq)
		return (0);
	bsq->x = x;
	bsq->y = y;
	bsq->size = size;
	return (bsq);
}

int			find_min(int **tab, int x, int y)
{
	int	min;

	min = 0;
	if (x == 0 || y == 0)
		return (0);
	min = tab[x - 1][y];
	if (tab[x - 1][y - 1] < min)
		min = tab[x - 1][y - 1];
	if (tab[x][y - 1] < min)
		return (tab[x][y - 1]);
	return (min);
}
