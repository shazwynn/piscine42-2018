/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_input.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 03:45:59 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 22:29:32 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int	add_first_line(char *line, int *check, t_grid *grid, int *obstacles)
{
	if (line[0] == '\0')
	{
		*check = 0;
		return (0);
	}
	grid->grid = ft_strdup(line);
	grid->columns = ft_strlen(grid->grid);
	grid->grid = ft_strjoin(grid->grid, "\n");
	if (!check_line(grid->grid, grid, &obstacles))
		*check = 0;
	return (0);
}

int	read_first_line(int fd, t_grid *grid, int *check, int *obstacles)
{
	char	buf[2];
	char	*line;
	char	*keep;

	if (!(line = (char *)malloc(sizeof(char) * 2)))
		return (0);
	buf[1] = '\0';
	while (read(fd, buf, BUF_SIZE) && buf[0] != '\n')
	{
		if (!keep && !line)
			line[0] = buf[0];
		else
		{
			keep = ft_strdup(line);
			ft_bufclear(line, ft_strlen(line));
			free(line);
			line = ft_strjoin(keep, buf);
			free(keep);
		}
	}
	add_first_line(line, check, grid, obstacles);
	free(line);
	return (0);
}

int	read_rest(int fd, t_grid *grid, int *check, int *obstacles)
{
	char	*buf;
	int		i;
	int		ret;

	if (!(buf = (char *)malloc(sizeof(char) * (grid->columns + 2))))
		return (0);
	i = 1;
	ft_bufclear(buf, grid->columns + 2);
	while ((ret = read(fd, buf, grid->columns + 1)))
	{
		if (ret == grid->columns + 1)
		{
			if (!check_line(buf, grid, &obstacles))
				*check = 0;
			grid->grid = ft_strjoin(grid->grid, buf);
		}
		else
			*check = 0;
		i++;
		ft_bufclear(buf, grid->columns + 1);
	}
	if (i != grid->lines)
		*check = 0;
	free(buf);
	return (0);
}

int	init_read(int fd, t_grid *grid, int *check, int *obstacles)
{
	read_instructions(fd, grid, check);
	if (*check == 0)
		return (-1);
	read_first_line(fd, grid, check, obstacles);
	if (*check == 0)
		return (-1);
	read_rest(fd, grid, check, obstacles);
	if (*check == 0)
		return (-1);
	return (0);
}

int	read_input(int fd)
{
	int			check;
	int			obstacles;
	t_deathstar	*bsq;
	int			**tab;
	t_grid		*grid;

	check = 1;
	obstacles = 0;
	grid = malloc(sizeof(t_grid));
	if (init_read(fd, grid, &check, &obstacles) == -1)
		return (-1);
	if (obstacles == grid->lines)
	{
		ft_putstr(grid->grid);
		return (0);
	}
	tab = NULL;
	bsq = NULL;
	bsq = create_bsq(bsq, 0, 0, 0);
	tab = create_tab(grid->columns);
	find_bsq(grid, &bsq, tab, grid->grid);
	display_grid_bsq(grid, bsq);
	free_utils(bsq, tab, grid);
	return (0);
}
