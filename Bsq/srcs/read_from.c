/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_from.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 18:17:29 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 22:09:00 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

/*
** open_file returns fd or -1 if open failed
*/

int	open_file(char *file)
{
	int	fd;

	if (open(file, O_DIRECTORY) != -1)
		return (-1);
	fd = open(file, O_RDONLY);
	return (fd);
}

/*
**	read_from_file returns -1 if open failed
*/

int	read_from_file(char *file)
{
	int	fd;

	fd = open_file(file);
	if (fd == -1)
		return (-1);
	if (read_input(fd) == -1)
	{
		return (-1);
		close(fd);
	}
	else
		close(fd);
	return (0);
}

/*
**	read_from_stdin if no file is given
*/

int	read_from_stdin(void)
{
	if (read_input(0) == -1)
		return (-1);
	return (0);
}

int	read_instructions(int fd, t_grid *grid, int *check)
{
	char	buffer;
	char	*line;
	int		i;

	i = 0;
	line = (char *)malloc(sizeof(char) * 15);
	if (!line)
		return (0);
	while (read(fd, &buffer, BUF_SIZE) && buffer != '\n' && i < 14)
		line[i++] = buffer;
	line[i] = '\0';
	if (!check_instructions(line, grid))
		*check = 0;
	ft_bufclear(line, 15);
	free(line);
	return (0);
}
