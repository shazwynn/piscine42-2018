/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/27 19:51:07 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 05:51:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		ft_atoi(char *str)
{
	int	res;
	int i;
	int s;

	i = 0;
	res = 0;
	s = 0;
	while (str[s] == ' ' || str[s] <= 31 || str[s] >= 127)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] >= '0' && str[i + s] <= '9')
	{
		res = res * 10 + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

void	ft_bufclear(char *buf, int buf_size)
{
	int	i;

	i = 0;
	while (i < buf_size + 1)
	{
		buf[i] = '\0';
		i++;
	}
}

char	*ft_strdup(char *src)
{
	int		i;
	char	*copy;

	i = 0;
	while (src[i])
		i++;
	if (!(copy = (char*)malloc(sizeof(char) * (i + 1))))
		return (copy);
	i = 0;
	while (src[i])
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}

char	*ft_strjoin(char *s1, char *s2)
{
	char	*join;
	int		i;
	int		j;
	int		len;

	join = NULL;
	i = 0;
	j = 0;
	len = ft_strlen(s1) + ft_strlen(s2);
	if (!s1 || !s2)
		return (join);
	join = (char *)malloc(sizeof(char) * (len + 1));
	if (!join)
		return (join);
	while (s1[i])
	{
		join[i] = s1[i];
		i++;
	}
	while (s2[j])
		join[i++] = s2[j++];
	join[i] = '\0';
	return (join);
}
