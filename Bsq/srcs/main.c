/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 01:52:50 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 18:16:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int	main(int argc, char **argv)
{
	int	i;

	i = 1;
	if (argc == 1)
	{
		if (read_from_stdin() == -1)
			ft_putstr("map error\n");
		return (0);
	}
	while (i < argc)
	{
		if (read_from_file(argv[i]) == -1)
			ft_putstr("map error\n");
		i++;
		if (i < argc)
			ft_putchar('\n');
	}
	return (0);
}
