/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks_read.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 17:04:20 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 21:37:43 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

/*
** check_instructions will return a struct with all instructions
*/

int		check_instructions(char *str, t_grid *grid)
{
	int		i;
	int		j;
	int		nb;

	i = 0;
	j = 0;
	nb = 0;
	while (str[i])
		i++;
	while (str[j] && j < i - 3 && str[j] >= '0' && str[j] <= '9')
		nb = nb * 10 + str[j++] - '0';
	i--;
	if (str[i] == str[i - 1] || str[i] == str[i - 2] ||
			str[i - 1] == str[i - 2])
		return (0);
	if (nb <= 0)
		return (0);
	grid->lines = nb;
	grid->full = str[i--];
	grid->obstacle = str[i--];
	grid->empty = str[i--];
	return (1);
}

/*
** check_line will return 1 if line read is valid, 0 otherwise
*/

int		check_line(char *line, t_grid *grid, int **obstacles)
{
	int len;
	int i;

	len = 0;
	i = 0;
	while (line[len] != '\n')
		len++;
	if (len == grid->columns && line[len] == '\n')
	{
		while (line[i] == grid->obstacle)
			i++;
		if (len == i)
		{
			**obstacles = **obstacles + 1;
			return (2);
		}
		i = -1;
		while (line[++i] != '\n')
		{
			if (!(line[i] == grid->empty || line[i] == grid->obstacle))
				return (0);
		}
		return (1);
	}
	return (0);
}
