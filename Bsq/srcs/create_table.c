/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_table.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tsisadag <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 23:15:50 by tsisadag          #+#    #+#             */
/*   Updated: 2018/02/28 20:07:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		**create_tab(int x)
{
	int **tab;
	int i;

	i = 0;
	tab = (int **)malloc(sizeof(int*) * 2);
	tab[0] = (int *)malloc(sizeof(int) * x);
	tab[1] = (int *)malloc(sizeof(int) * x);
	return (tab);
}

int		**swap_tab(int **tab)
{
	int *tmp0;
	int *tmp1;

	tmp0 = tab[0];
	tmp1 = tab[1];
	tab[0] = tmp1;
	tab[1] = tmp0;
	return (tab);
}
