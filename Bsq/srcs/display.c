/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 15:19:24 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 21:38:11 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			ft_putchar(str[i]);
			i++;
		}
	}
}

int		is_square_coord(int x, int y, t_deathstar *bsq)
{
	int	i;
	int j;

	i = 0;
	while (i < bsq->size)
	{
		j = 0;
		while (j < bsq->size)
		{
			if (x == bsq->x - i && y == bsq->y - j)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

void	display_grid_bsq(t_grid *grid, t_deathstar *bsq)
{
	int		i;
	int		j;
	char	*tmp;

	i = 0;
	j = 0;
	tmp = grid->grid;
	while (tmp[i])
	{
		if (is_square_coord(i, j, bsq))
		{
			ft_putchar(grid->full);
			i++;
		}
		else if (tmp[i] == '\n')
		{
			write(1, &tmp[i], 1);
			tmp = tmp + i + 1;
			i = 0;
			j++;
		}
		else
			write(1, &tmp[i++], 1);
	}
}
