/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 13:10:00 by algrele           #+#    #+#             */
/*   Updated: 2018/02/28 22:28:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# define BUF_SIZE 1

typedef	struct	s_grid
{
	char		*grid;
	int			columns;
	int			lines;
	char		empty;
	char		obstacle;
	char		full;
}				t_grid;

typedef struct	s_deathstar
{
	int			x;
	int			y;
	int			size;
}				t_deathstar;
/*
** DISPLAY
*/

void			ft_putchar(char c);
void			ft_putstr(char *str);
void			display_grid_bsq(t_grid *grid, t_deathstar *bsq);
int				is_square_coord(int x, int y, t_deathstar *bsq);
void			bsq_to_grid(t_grid *grid, t_deathstar *bsq);

/*
** READ_INPUT
*/

int				read_first_line(int fd, t_grid *grid, int *check,
				int *obstacles);
int				read_input(int fd);
int				init_read(int fd, t_grid *grid, int *check, int *obstacles);
int				read_rest(int fd, t_grid *grid, int *check, int *obstacles);

/*
** READ_FROM
*/

int				open_file(char *file);
int				read_from_file(char *file);
int				read_from_stdin(void);
int				read_instructions(int fd, t_grid *grid, int *check);

/*
** CHECKS_READ
*/

int				check_instructions(char *str, t_grid *grid);
int				check_line(char *line, t_grid *grid, int **obstacles);

/*
** BSQ
*/

int				switch_lines(int **tab, int *cur, int *i);
void			find_bsq(t_grid *grid, t_deathstar **bsq, int **tab, char *tmp);
t_deathstar		*create_bsq(t_deathstar *bsq, int x, int y, int size);
int				find_min(int **tab, int x, int y);

/*
** CREATE_TABLE
*/

int				**create_tab(int x);
int				**swap_tab(int **tab);

/*
** STR_TOOLS
*/

int				ft_atoi(char *str);
int				ft_strlen(char *str);
void			ft_bufclear(char *buf, int buf_size);
char			*ft_strdup(char *src);
char			*ft_strjoin(char *s1, char *s2);

/*
** TOOLS
*/

void			free_utils(t_deathstar *bsq, int **tab, t_grid *grid);

#endif
