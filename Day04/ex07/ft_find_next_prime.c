/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 23:54:23 by algrele           #+#    #+#             */
/*   Updated: 2018/02/12 04:38:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	my_sqrt(int nb)
{
	int i;

	i = 1;
	while (i * i < nb && i <= 46341)
		i++;
	return (i);
}

int	my_is_prime(int nb)
{
	int i;
	int	sqrt;

	i = 2;
	if (nb <= 0 || nb == 1)
		return (0);
	if (nb == 2)
		return (1);
	sqrt = my_sqrt(nb);
	while (i <= sqrt)
	{
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (1);
}

int	ft_find_next_prime(int nb)
{
	int i;

	i = 0;
	if (nb <= 2)
		return (2);
	while (my_is_prime(nb + i) != 1)
		i++;
	return (nb + i);
}
