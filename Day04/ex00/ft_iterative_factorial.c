/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 02:18:51 by algrele           #+#    #+#             */
/*   Updated: 2018/02/07 03:07:49 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int fact;
	int i;

	if (nb == 0)
		return (1);
	else if (nb < 13 && nb > 0)
	{
		i = 1;
		fact = 1;
		while (i <= nb)
		{
			fact = i * fact;
			i++;
		}
		return (fact);
	}
	return (0);
}
