/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle_2.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 04:47:15 by algrele           #+#    #+#             */
/*   Updated: 2018/02/12 21:10:04 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_tab(int *tab)
{
	int i;

	i = 0;
	while (i < 8)
	{
		ft_putchar(tab[i] + '0' + 1);
		i++;
	}
	ft_putchar('\n');
}

int		ft_is_valid_2(int *tab, int column, int pos)
{
	int i;

	i = 0;
	while (i < column)
	{
		if (pos == tab[i])
			return (0);
		if (i + tab[i] == column + pos)
			return (0);
		if (i - tab[i] == column - pos)
			return (0);
		i++;
	}
	return (1);
}

void	ft_place_queens_2(int *tab, int pos)
{
	int i;

	i = 0;
	if (pos == 8)
		ft_print_tab(tab);
	else
	{
		while (i < 8)
		{
			if (ft_is_valid_2(tab, pos, i))
			{
				tab[pos] = i;
				ft_place_queens_2(tab, pos + 1);
			}
			i++;
		}
	}
}

void	ft_eight_queens_puzzle_2(void)
{
	int tab[8];
	int i;

	i = 1;
	while (i++ < 8)
		tab[i] = 0;
	ft_place_queens_2(tab, 0);
}
