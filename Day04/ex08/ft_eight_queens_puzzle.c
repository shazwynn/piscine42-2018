/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 01:11:44 by algrele           #+#    #+#             */
/*   Updated: 2018/02/12 04:25:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_valid(int *tab, int column, int pos)
{
	int i;

	i = 0;
	while (i < column)
	{
		if (pos == tab[i])
			return (0);
		if (i + tab[i] == column + pos)
			return (0);
		if (i - tab[i] == column - pos)
			return (0);
		i++;
	}
	return (1);
}

void	ft_place_queens(int *tab, int *res, int pos)
{
	int i;

	i = 0;
	if (pos == 8)
		*res = *res + 1;
	else
	{
		while (i < 8)
		{
			if (ft_is_valid(tab, pos, i))
			{
				tab[pos] = i;
				ft_place_queens(tab, res, pos + 1);
			}
			i++;
		}
	}
}

int		ft_eight_queens_puzzle(void)
{
	int tab[8];
	int res;
	int i;

	res = 0;
	i = 1;
	while (i++ < 8)
		tab[i] = 0;
	ft_place_queens(tab, &res, 0);
	return (res);
}
