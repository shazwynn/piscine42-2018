/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 15:12:02 by algrele           #+#    #+#             */
/*   Updated: 2018/02/08 16:58:15 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int i;
	int ret;

	i = 0;
	ret = 1;
	if (power < 0)
		return (0);
	while (i < power)
	{
		ret = nb * ret;
		i++;
	}
	return (ret);
}
