/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 12:34:50 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 05:18:40 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RUSH02_H
# define RUSH02_H

# include <stdlib.h>
# include <unistd.h>
# define BUF_SIZE 30

/*
** DISPLAY
*/

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nb);
void	ft_trouve(char colle, int x, int y);
int		ft_trouve_tout(void);

/*
** STR_TOOLS
*/

int		ft_strlen(char *str);
void	ft_bufclear(char *buf, int buf_size);
char	*ft_strdup(char *src);
char	*ft_strjoin(char *s1, char *s2);
char	*ft_tri(char *ten, char *triplet, int start);

/*
** READ_INPUT
*/

char	*read_input(void);

/*
** FT_COUNT_X_Y
*/

int		ft_count_x_y(char *result, int x, int y);

/*
** CREATE_COLLE
*/

int		check_first_char(char *result, int x, int y);
int		ft_first_a(int *tab, int x, int y);

/*
** CMP_RUSH
*/

int		ft_line(char *result, int x, char *triplet);
int		cmprush(char *result, int x, int y, char *nine);

#endif
