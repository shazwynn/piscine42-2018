/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmprush00.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 19:01:27 by lfatton           #+#    #+#             */
/*   Updated: 2018/02/25 01:19:44 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int		ft_line(char *res, int x, char *tri)
{
	int i;
	int j;

	i = 1;
	j = 0;
	while (i <= x && res[j])
	{
		if (i == 1 && res[j] != tri[0])
			return (-1);
		else if (x != 1 && i == x && res[j] != tri[2])
			return (-1);
		else if (x != 1 && i != 1 && i != x && res[j] != tri[1])
			return (-1);
		i++;
		j++;
	}
	if (i > 1 && res[j] != '\n')
		return (-1);
	return (0);
}

int		cmprush(char *res, int x, int y, char *ten)
{
	int		i;
	char	*tri;

	tri = (char*)malloc(sizeof(char) * 4);
	tri = ft_tri(ten, tri, 1);
	i = 1;
	while (i <= y)
	{
		if (i == 1 && (ft_line(res, x, tri) == -1))
			return (-1);
		if (y != 1 && i == y && (ft_line(res, x, ft_tri(ten, tri, 7)) == -1))
			return (-1);
		if (y != 1 && i != y && i != 1)
			if (ft_line(res, x, ft_tri(ten, tri, 4)) == -1)
				return (-1);
		i++;
		res = res + x + 1;
	}
	free(tri);
	if (ten[0] == '0' || ten[0] == '1')
		ft_trouve(ten[0], x, y);
	return (1);
}
