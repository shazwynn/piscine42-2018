/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_colle.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 17:01:02 by lfatton           #+#    #+#             */
/*   Updated: 2018/02/25 01:26:29 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int	check_first_char(char *result, int x, int y)
{
	int		i;
	int		tab[3];

	i = 0;
	if (result[i] == 'o')
		return (cmprush(result, x, y, "0o-o| |o-o"));
	if (result[i] == 47)
		return (cmprush(result, x, y, "1/*\\* *\\*/"));
	if (result[i] == 'A')
	{
		if (x == 1 && y == 1)
			return (ft_trouve_tout());
		tab[0] = cmprush(result, x, y, "2ABAB BCBC");
		tab[1] = cmprush(result, x, y, "3ABCB BABC");
		tab[2] = cmprush(result, x, y, "4ABCB BCBA");
		return (ft_first_a(tab, x, y));
	}
	else
		return (-1);
}

int	ft_first_a(int *tab, int x, int y)
{
	if (tab[0] == -1 && tab[1] == -1 && tab[2] == -1)
		return (-1);
	if (tab[0] == 1)
		ft_trouve('2', x, y);
	if (tab[0] == 1 && tab[1] == 1)
		ft_putstr(" || ");
	if (tab[1] == 1)
		ft_trouve('3', x, y);
	if ((tab[1] == 1 || tab[0] == 1) && tab[2] == 1)
		ft_putstr(" || ");
	if (tab[2] == 1)
		ft_trouve('4', x, y);
	return (1);
}
