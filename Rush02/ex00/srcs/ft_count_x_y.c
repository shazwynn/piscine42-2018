/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_x_y.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qhoareau <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 18:27:09 by qhoareau          #+#    #+#             */
/*   Updated: 2018/02/25 17:31:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int		ft_count_x_y(char *result, int x, int y)
{
	int i;
	int check;

	i = -1;
	check = 0;
	while (result[++i])
	{
		if (result[i] != '\n' && y == 0)
			x++;
		if (result[i] != '\n' && y > 0)
			check++;
		if (result[i] == '\n')
		{
			if ((check < x && y > 0) || (check > x && y > 0))
				return (-1);
			if (check == x)
				check = 0;
			y++;
		}
	}
	if (i == 0 || (x <= 0 || y <= 0) || check_first_char(result, x, y) == -1)
		return (-1);
	return (1);
}
