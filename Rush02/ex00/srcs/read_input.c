/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_input.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 05:17:49 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 05:38:55 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

char	*read_input(void)
{
	char	*result;
	char	*keep;
	char	*buf;

	if (!(buf = (char *)malloc(sizeof(char) * (BUF_SIZE + 1))))
		return (0);
	if (!(result = (char *)malloc(sizeof(char) * 2)))
		return (0);
	ft_bufclear(buf, BUF_SIZE);
	while (read(0, buf, BUF_SIZE) > 0)
	{
		if (!keep && !result)
			result = ft_strdup(buf);
		else
		{
			keep = ft_strdup(result);
			result = ft_strjoin(keep, buf);
			free(keep);
		}
		ft_bufclear(buf, BUF_SIZE);
	}
	free(buf);
	return (result);
}
