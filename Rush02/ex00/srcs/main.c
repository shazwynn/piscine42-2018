/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 20:46:01 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 17:31:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int	main(void)
{
	char	*result;

	result = read_input();
	if (!result || ft_count_x_y(result, 0, 0) == -1)
		ft_putstr("aucune");
	ft_putchar('\n');
	free(result);
	return (0);
}
