/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 12:59:33 by algrele           #+#    #+#             */
/*   Updated: 2018/02/25 04:33:28 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush02.h"

int		ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

void	ft_bufclear(char *buf, int buf_size)
{
	int	i;

	i = 0;
	while (i < buf_size + 1)
	{
		buf[i] = '\0';
		i++;
	}
}

char	*ft_strdup(char *src)
{
	int		i;
	char	*copy;

	i = 0;
	while (src[i])
		i++;
	if (!(copy = (char*)malloc(sizeof(char) * (i + 1))))
		return (copy);
	i = 0;
	while (src[i])
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}

char	*ft_strjoin(char *s1, char *s2)
{
	char	*join;
	int		i;
	int		j;
	int		len;

	join = NULL;
	i = 0;
	j = 0;
	len = ft_strlen(s1) + ft_strlen(s2);
	if (!s1 || !s2)
		return (join);
	join = (char *)malloc(sizeof(char) * (len + 1));
	if (!join)
		return (join);
	while (s1[i])
	{
		join[i] = s1[i];
		i++;
	}
	while (s2[j])
		join[i++] = s2[j++];
	join[i] = '\0';
	return (join);
}

char	*ft_tri(char *ten, char *triplet, int start)
{
	triplet[0] = ten[start];
	triplet[1] = ten[start + 1];
	triplet[2] = ten[start + 2];
	triplet[3] = '\0';
	return (triplet);
}
