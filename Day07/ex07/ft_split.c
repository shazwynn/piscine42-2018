/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 03:06:25 by algrele           #+#    #+#             */
/*   Updated: 2018/02/21 14:23:34 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*split_strndup(char *src, int start, int size)
{
	int		i;
	char	*copy;

	copy = (char *)malloc(sizeof(char) * (size + 1));
	if (copy)
	{
		i = 0;
		while (src[i + start] && i < size)
		{
			copy[i] = src[i + start];
			i++;
		}
		copy[i] = '\0';
	}
	return (copy);
}

int		is_in_charset(char c, char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int		count_words(char *str, char *charset)
{
	int	count;
	int	i;

	count = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] && is_in_charset(str[i], charset))
			i++;
		if (str[i])
		{
			count++;
			while (str[i] && is_in_charset(str[i], charset) == 0)
				i++;
		}
	}
	return (count);
}

char	**ft_split(char *str, char *charset)
{
	char	**tab;
	int		words;
	int		i;
	int		k;

	tab = (char **)malloc(sizeof(char *) * (count_words(str, charset) + 1));
	if (!tab)
		return (NULL);
	i = 0;
	words = 0;
	while (str[i])
	{
		k = 0;
		while (str[i] && is_in_charset(str[i], charset))
			i++;
		while (str[i + k] && is_in_charset(str[i + k], charset) == 0)
			k++;
		if (k > 0)
			tab[words++] = split_strndup(str, i, k);
		i = i + k;
	}
	tab[words] = 0;
	return (tab);
}
