/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 00:45:13 by algrele           #+#    #+#             */
/*   Updated: 2018/02/21 17:14:36 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		c_ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

int		ft_total(int argc, char **argv)
{
	int	total;
	int	i;

	i = 1;
	total = 0;
	while (i < argc)
	{
		total = total + c_ft_strlen(argv[i]);
		if (i + 1 != argc)
			total++;
		i++;
	}
	return (total);
}

char	*ft_concat_params(int argc, char **argv)
{
	char	*str;
	int		i;
	int		j;
	int		k;

	if (argc == 0)
		return (0);
	str = (char *)malloc(sizeof(char) * ft_total(argc, argv) + 1);
	i = 1;
	j = 0;
	k = 0;
	if (!str)
		return (0);
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
			str[k++] = argv[i][j++];
		if (i < argc - 1)
			str[k++] = '\n';
		i++;
	}
	str[k] = '\0';
	return (str);
}
