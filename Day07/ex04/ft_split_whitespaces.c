/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 01:32:18 by algrele           #+#    #+#             */
/*   Updated: 2018/02/20 02:19:05 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*split_strndup(char *src, int start, int size)
{
	int		i;
	char	*copy;

	copy = (char *)malloc(sizeof(char) * (size + 1));
	if (copy)
	{
		i = 0;
		while (src[i + start] && i < size)
		{
			copy[i] = src[i + start];
			i++;
		}
		copy[i] = '\0';
	}
	return (copy);
}

int		count_words(char *str)
{
	int	count;
	int	i;

	count = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
			i++;
		if (str[i])
		{
			count++;
			while (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
				i++;
		}
	}
	return (count);
}

char	**ft_split_whitespaces(char *str)
{
	char	**tab;
	int		words;
	int		i;
	int		k;

	tab = (char **)malloc(sizeof(char *) * (count_words(str) + 1));
	if (!tab)
		return (NULL);
	i = 0;
	words = 0;
	while (str[i])
	{
		k = 0;
		while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
			i++;
		while (str[i + k] && str[i + k] != ' ' && str[i + k] != '\t'
				&& str[i + k] != '\n')
			k++;
		if (k > 0)
			tab[words++] = split_strndup(str, i, k);
		i = i + k;
	}
	tab[words] = 0;
	return (tab);
}
