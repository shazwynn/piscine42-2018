/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putcolor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrele <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 19:05:52 by agrele            #+#    #+#             */
/*   Updated: 2018/02/07 23:06:02 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <libft.h>
#include "print.h"


void	ft_putstr(char *str);
void	ft_putchar(char c);
void	ft_putnbr(int nb);
int		ft_atoi(char *str);

void	ft_putstr_c(char *str, char *color)
{
	int		x;

	x = ft_atoi(color);
	if (x > 0 && x < 256)
	{
		ft_putstr("\e[38;5;");
		ft_putnbr(x);
		ft_putstr("m");
	}
	else if (color[1] == '[')
		ft_putstr(color);
	ft_putstr(str);
	ft_putstr(RESET_COLOR);
}

void	ft_putendl_c(char *str, char *color)
{
	ft_putstr_c(str, color);
	ft_putchar('\n');
}

void	ft_putchar_c(char c, char *color)
{
	int		x;

	x = ft_atoi(color);
	if (x > 0 && x < 256)
	{
		ft_putstr("\e[38;5;");
		ft_putnbr(x);
		ft_putstr("m");
	}
	else if (color[1] == '[')
		ft_putstr(color);
	ft_putchar(c);
	ft_putstr(RESET_COLOR);
}

void	ft_putnbr_c(int n, char *color)
{
	int		x;

	x = ft_atoi(color);
	if (x > 0 && x < 256)
	{
		ft_putstr("\e[38;5;");
		ft_putnbr(x);
		ft_putstr("m");
	}
	else if (color[1] == '[')
		ft_putstr(color);
	ft_putnbr(n);
	ft_putstr(RESET_COLOR);
}

void	ft_putnbrl_c(int n, char *color)
{
	ft_putnbr_c(n, color);
	ft_putchar('\n');
}
