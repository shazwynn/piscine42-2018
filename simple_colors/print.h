#ifndef PRINT_H
# define PRINT_H

# include <unistd.h>

# define UNDERLINE "\e[38;4;2m"
# define BACKGROUND "\e[48;5;226m"

/*
** COLOR MACROS : P for Pale, D for Dark, F for Fluo
*/

# define NONE "\033[0m"
# define BLACK "\e[38;5;16m"
# define RESET_ALL "\033[0m"
# define RESET_COLOR "\033[0m"

/*
** RED, PINK AND PURPLE
*/

# define P_RED "\e[38;5;13m"
# define RED "\e[38;5;196m"
# define D_RED "\e[38;5;124m"
# define F_RED "\e[38;5;196m"

# define P_PINK "\e[38;5;204m"
# define PINK "\e[38;5;198m"
# define D_PINK "\e[38;5;161m"
# define F_PINK "\e[38;5;199m"

# define P_PURPLE "\e[38;5;135m"
# define PURPLE "\e[38;5;128m"
# define D_PURPLE "\e[38;5;90m"
# define F_PURPLE "\e[38;5;197m"

/*
** BLUE, GREEN, YELLOW, ORANGE
*/

# define P_BLUE "\e[38;5;39m"
# define BLUE "\e[38;5;32m"
# define D_BLUE "\e[38;5;21m"
# define F_BLUE "\e[38;5;45m"

# define P_GREEN "\e[38;5;119m"
# define GREEN "\e[38;5;40m"
# define D_GREEN "\e[38;5;34m"
# define F_GREEN "\e[38;5;82m"

# define TURQUOISE "\e[38;5;36m"
# define YELLOW "\e[38;5;226m"
# define SUN "\e[38;5;208m"
# define ORANGE "\e[38;5;202m"

/*
** ARRAY MACROS
*/

# define COLOR1 "\e[38;5;13m"
# define COLOR2 "\e[38;5;249m"

/*
# define COLOR1 "\e[38;5;7m"
# define COLOR2 "\e[38;5;249m"

# define COLOR3 "\e[38;5;15m"
# define COLOR4 "\e[38;5;255m"

# define COLOR5 "\e[38;5;8m"
# define COLOR6 "\e[38;5;240m"

# define COLOR1 "\e[38;5;6m"
# define COLOR2 "\e[38;5;37m"

# define COLOR3 "\e[38;5;14m"
# define COLOR4 "\e[38;5;45m"

# define COLOR5 "\e[38;5;12m"
# define COLOR6 "\e[38;5;63m"
*/
# define R_ARRAY_1 "196.202.208.214.220.226.190.154.118.82.40."
# define R_ARRAY_2 "42.35.37.45.31.33.27.105.63.93.128."
# define R_ARRAY_3 "124.161.198.126.200.164.206.171.135.240.249.255."
# define SUNSET_ARRAY "220.209.202.124.196.198."

/*
** char, nbr, nbrl (nbr + '\n'), str, endl (str + '\n')
*/

/*void	ft_putcolor(char *message, char *color);*/
void	ft_putstr_c(char *str, char *color);
void	ft_putendl_c(char *str, char *color);
void	ft_putchar_c(char c, char *color);
void	ft_putnbr_c(int n, char *color);
void	ft_putnbrl_c(int n, char *color);
void	ft_putrainbow(char *str, char *color_palette);

#endif
