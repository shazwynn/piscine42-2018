/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 21:11:27 by algrele           #+#    #+#             */
/*   Updated: 2018/02/13 03:21:24 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_letnum(char c)
{
	if (c >= 'a' && c <= 'z')
		return (1);
	else if (c >= 'A' && c <= 'Z')
		return (2);
	else if (c >= '0' && c <= '9')
		return (3);
	else
		return (0);
}

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (ft_is_letnum(str[i]))
		{
			if (ft_is_letnum(str[i]) == 1)
				str[i] = str[i] - ' ';
			i++;
		}
		while (str[i] && ft_is_letnum(str[i]))
		{
			if ((ft_is_letnum(str[i]) == 2))
				str[i] = str[i] + ' ';
			i++;
		}
		while (str[i] && ft_is_letnum(str[i]) == 0)
			i++;
	}
	return (str);
}
