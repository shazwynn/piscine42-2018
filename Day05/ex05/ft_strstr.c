/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fr_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 06:51:14 by algrele           #+#    #+#             */
/*   Updated: 2018/02/19 17:23:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		j;
	char	*ptr;

	ptr = 0;
	i = 0;
	if (!to_find[i])
		return (str);
	else
	{
		while (str[i])
		{
			j = 0;
			while (str[i + j] == to_find[j])
				j++;
			i++;
			if (to_find[j] == '\0')
				return (&str[i - 1]);
		}
	}
	return (ptr);
}
