/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 23:11:36 by algrele           #+#    #+#             */
/*   Updated: 2018/02/19 17:19:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	int	res;
	int i;
	int s;

	i = 0;
	res = 0;
	s = 0;
	while (str[s] == ' ' || str[s] <= 31 || str[s] >= 127)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] >= '0' && str[i + s] <= '9')
	{
		res = res * 10 + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}
