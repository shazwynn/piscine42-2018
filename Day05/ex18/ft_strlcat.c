/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 01:02:36 by algrele           #+#    #+#             */
/*   Updated: 2018/02/19 17:28:59 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				sc_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	j;
	unsigned int	len;
	unsigned int	ret;

	len = sc_strlen(dest);
	ret = size;
	if (len > size)
		len = size;
	j = -1;
	while (src[++j] && size-- > len + 1)
		dest[j + len] = src[j];
	if (ret > j + len)
		dest[j + len] = '\0';
	return (sc_strlen(src) + len);
}
