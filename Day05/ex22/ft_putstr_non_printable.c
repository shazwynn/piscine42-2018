/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 17:54:47 by algrele           #+#    #+#             */
/*   Updated: 2018/02/19 19:49:04 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	np_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	np_recursive_number(long int nb, char *base, long int i_base)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= i_base)
		np_recursive_number(nb / i_base, base, i_base);
	ft_putchar(base[nb % i_base]);
}

void	np_putnbr_base(int nb, char *base)
{
	int		stock;
	int		i_base;

	i_base = 0;
	while (base && base[i_base])
		i_base++;
	stock = 0;
	np_recursive_number(nb, base, i_base);
}

void	ft_putstr_non_printable(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] < 32 || str[i] > 126)
		{
			ft_putchar('\\');
			if (str[i] < 16)
				ft_putchar('0');
			np_putnbr_base(str[i], "0123456789abcdef");
		}
		else
			ft_putchar(str[i]);
		i++;
	}
}
