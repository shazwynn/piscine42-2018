/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 07:34:46 by algrele           #+#    #+#             */
/*   Updated: 2018/02/20 07:59:26 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_display(char **tab)
{
	int	i;
	int j;

	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j])
		{
			ft_putchar(tab[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void	ft_putnbr(int nb)
{
	int		i;
	char	*smi;

	i = 0;
	smi = "-2147483648";
	if (nb == -2147483648)
		while (i < 11)
			ft_putchar(smi[i++]);
	else
	{
		if (nb < 0)
		{
			ft_putchar('-');
			nb = -nb;
		}
		if (nb >= 10)
		{
			ft_putnbr(nb / 10);
			ft_putnbr(nb % 10);
		}
		else
			ft_putchar(nb + '0');
	}
}

void	ft_show_tab(struct s_stock_par *par)
{
	int	i;

	if (par)
	{
		i = 0;
		while (par[i].str)
		{
			ft_putstr(par[i].str);
			ft_putchar('\n');
			ft_putnbr(par[i].size_param);
			ft_putchar('\n');
			ft_display(par[i].tab);
			i++;
		}
	}
}
