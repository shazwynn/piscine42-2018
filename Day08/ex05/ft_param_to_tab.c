/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 07:12:23 by algrele           #+#    #+#             */
/*   Updated: 2018/02/22 15:43:30 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

int					ft_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

char				*ft_strcpy(char *dst, char *src)
{
	int	i;

	i = 0;
	while (src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

char				*ft_strdup(char *src)
{
	int		i;
	char	*copy;

	i = 0;
	while (src[i])
		i++;
	copy = (char*)malloc(sizeof(char) * (i + 1));
	if (copy)
	{
		ft_strcpy(copy, src);
	}
	return (copy);
}

struct s_stock_par	*ft_param_to_tab(int argc, char **argv)
{
	struct s_stock_par	*t;
	int					i;

	t = malloc(sizeof(struct s_stock_par) * (argc + 1));
	if (!t)
		return (t);
	i = 0;
	while (i < argc)
	{
		t[i].size_param = ft_strlen(argv[i]);
		t[i].str = argv[i];
		t[i].copy = ft_strdup(argv[i]);
		t[i].tab = ft_split_whitespaces(argv[i]);
		i++;
	}
	t[i].str = 0;
	return (t);
}
