/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 22:25:22 by algrele           #+#    #+#             */
/*   Updated: 2018/02/06 23:08:56 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char		*ft_strrev(char *str)
{
	int		i;
	int		len;
	int		half_len;
	char	tmp;

	i = 0;
	len = 0;
	while (str[len])
		len++;
	if (len > 0)
	{
		half_len = len / 2;
		while (i < half_len)
		{
			tmp = str[i];
			str[i] = str[len - 1];
			str[len - 1] = tmp;
			i++;
			len--;
		}
	}
	return (str);
}
