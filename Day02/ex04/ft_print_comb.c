/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 23:16:29 by algrele           #+#    #+#             */
/*   Updated: 2018/02/06 17:26:02 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_comb(void)
{
	int a;
	int b;
	int c;

	a = -1;
	while (a++ <= 7)
	{
		b = a;
		while (b++ <= 8)
		{
			c = b + 1;
			while (c++ <= 9)
			{
				ft_putchar(a + '0');
				ft_putchar(b + '0');
				ft_putchar(c - 1 + '0');
				if (a != 7 || b != 8 || c - 1 != 9)
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
			}
		}
	}
}
