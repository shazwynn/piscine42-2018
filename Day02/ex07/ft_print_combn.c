/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 12:05:09 by algrele           #+#    #+#             */
/*   Updated: 2018/02/13 21:53:35 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_tab(int *tab, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		ft_putchar(tab[i] + '0');
		i++;
	}
	if (tab[0] != 10 - len)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

int		ft_check_tab(int *tab, int len)
{
	int	i;
	int j;

	j = 0;
	if (!tab || !len)
		return (0);
	while (j < len)
	{
		i = j + 1;
		while (i < len)
		{
			if (tab[i] <= tab[j])
				return (0);
			i++;
		}
		j++;
	}
	return (1);
}

void	ft_print_combn(int n)
{
	int	tab[10];
	int i;

	i = 0;
	while (n > 0 && n < 10 && 10 > i++)
		tab[i - 1] = 0;
	while (n > 0 && n < 10 && tab[0] <= 10 - n)
	{
		if (ft_check_tab(tab, n) != 0)
			ft_print_tab(tab, n);
		tab[n - 1]++;
		i = n;
		while (i > 1)
		{
			i--;
			if (tab[i] > 9)
			{
				tab[i - 1]++;
				tab[i] = 0;
			}
		}
	}
}
