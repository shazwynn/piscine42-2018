/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 10:11:44 by algrele           #+#    #+#             */
/*   Updated: 2018/02/06 11:00:05 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_digits(int a, int b, int c, int d)
{
	if ((!(a == c && b == d)) && ((a * 10 + b) < (c * 10 + d)))
	{
		ft_putchar(a + '0');
		ft_putchar(b + '0');
		ft_putchar(' ');
		ft_putchar(c + '0');
		ft_putchar(d + '0');
		if (!(a == 9 && b == 8 && c == 9 && d == 9))
		{
			ft_putchar(',');
			ft_putchar(' ');
		}
	}
}

void	ft_print_comb2(void)
{
	int a;
	int b;
	int c;
	int d;

	a = -1;
	while (a++ <= 9)
	{
		b = 0;
		while (b <= 9)
		{
			c = 0;
			while (c <= 9)
			{
				d = 0;
				while (d <= 9)
					ft_print_digits(a, b, c, d++);
				c++;
			}
			b++;
		}
	}
}
