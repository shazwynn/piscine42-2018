#!/bin/sh
curl -sz annuaire https://projects.intra.42.fr/uploads/document/document/266/contacts_hard.txt --output annuaire
cat annuaire | grep -E -i "^nicolas	bauer|^bauer	nicolas" | grep -E -o "(\d){3}\.(\d){3}\.(\d)\.(\d){2}"
cat annuaire | grep -E -i "^nicolas	bauer|^bauer	nicolas" | grep -E -o "(\(?(\d){3}\)?)?-?(\d){3}-(\d){2,4}"
