/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rot42.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 21:31:55 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 12:49:51 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_rot42(char *str)
{
	int	i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			if (str[i] >= 'a' && str[i] < 'k')
				str[i] = str[i] + 16;
			else if (str[i] >= 'k' && str[i] <= 'z')
				str[i] = str[i] - 10;
			if (str[i] >= 'A' && str[i] < 'K')
				str[i] = str[i] + 16;
			else if (str[i] >= 'K' && str[i] <= 'Z')
				str[i] = str[i] - 10;
			i++;
		}
	}
	return (str);
}
