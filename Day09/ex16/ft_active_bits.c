/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_active_bits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 15:38:48 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 16:33:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_iterative_power(int nb, int power)
{
	int i;
	int resultat;

	i = 0;
	resultat = 1;
	if (power < 0)
	{
		return (0);
	}
	else
	{
		while (i < power)
		{
			resultat = (resultat * nb);
			i++;
		}
		return (resultat);
	}
}

unsigned int	ft_active_bits(int value)
{
	int bitactif;
	int nbit;
	int neg;

	bitactif = 0;
	neg = 0;
	if (value < 0)
	{
		neg = 32;
		value = -(value - 1);
	}
	nbit = 30;
	while (nbit >= 0)
	{
		if (value >= ft_iterative_power(2, nbit))
		{
			bitactif++;
			value = value - ft_iterative_power(2, nbit);
		}
		nbit--;
	}
	if (neg > 0)
		return (neg - bitactif);
	return (bitactif);
}
