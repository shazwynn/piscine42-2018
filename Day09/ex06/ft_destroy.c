/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destroy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 13:40:36 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 16:23:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_ultimator.h"

void	ft_destroy(char ***factory)
{
	int i;
	int j;

	i = 0;
	if (factory)
	{
		while (factory[i] != NULL)
		{
			j = 0;
			while (factory[i][j] != NULL)
			{
				free(factory[i][j]);
				j++;
			}
			free(factory[i]);
			i++;
		}
		free(factory);
	}
}
