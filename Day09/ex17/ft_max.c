/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 15:53:10 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 16:32:47 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_max(int *tab, int length)
{
	int i;
	int max;

	max = 0;
	i = 0;
	if (length == 1 && tab)
		return (tab[0]);
	if (length > 1 && tab)
	{
		max = tab[i];
		while (i < length)
		{
			if (tab[i] > max)
				max = tab[i];
			i++;
		}
		return (max);
	}
}
