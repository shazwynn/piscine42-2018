#!/bin/sh
THEIP=`ifconfig | grep -o -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v -e "255$" | grep -v -e "^127" | grep -v -e "^0"`

if [[ -z $THEIP ]]
then
	echo "Je suis perdu!\n"
else
	echo $THEIP
fi
