/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 13:59:49 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 14:10:06 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_spy(char *arg, char *word)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (arg[i] <= 32)
		i++;
	while (arg[i + j] == word[j] || arg[i + j] == word[j] - 32)
	{
		j++;
		if (word[j] == '\0')
		{
			while (arg[i + j] <= 32 && arg[i + j] > 0)
				i++;
			if (arg[i + j] == '\0')
			{
				write(1, "Alert!!!\n", 9);
				return (1);
			}
		}
	}
	return (0);
}

int		main(int argc, char **argv)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < argc && j == 0)
	{
		if (ft_spy(argv[i], "president") == 1)
			return (0);
		else if (ft_spy(argv[i], "bauer") == 1)
			return (0);
		else if (ft_spy(argv[i], "attack") == 1)
			return (0);
		i++;
	}
	return (0);
}
