/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjecture.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 13:58:03 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 13:59:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	if (base == 1)
		return (0);
	else if (base % 2 == 0)
	{
		base = ft_collatz_conjecture(base / 2);
		base++;
	}
	else
	{
		base = ft_collatz_conjecture(base * 3 + 1);
		base++;
	}
	return (base);
}
