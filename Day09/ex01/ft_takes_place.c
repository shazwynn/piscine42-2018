/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 18:12:34 by algrele           #+#    #+#             */
/*   Updated: 2018/02/15 19:27:19 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	int compteur;
	int temp;

	compteur = 1;
	if (n < 0)
	{
		ft_putchar('-');
		if (n == -2147483648)
		{
			n = -147483648;
			ft_putchar('2');
		}
		n = -n;
	}
	temp = n;
	while ((temp /= 10) > 0)
	{
		compteur *= 10;
	}
	while (compteur > 0)
	{
		ft_putchar(n / compteur + '0');
		n %= compteur;
		compteur /= 10;
	}
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_time(int hour)
{
	int time;

	if (hour > 12)
		time = hour - 12;
	else
		time = hour;
	ft_putnbr(time);
	ft_putchar('.');
	ft_putnbr(0);
	ft_putnbr(0);
	if ((hour > 11) && (hour != 24))
		ft_putstr(" P.M.");
	else
		ft_putstr(" A.M.");
}

void	ft_takes_place(int hour)
{
	if (hour == 0)
		hour = 24;
	if ((hour > 0) && (hour < 25))
	{
		ft_putstr("THE FOLLOWING TAKES PLACE BETWEEN ");
		ft_time(hour);
		ft_putstr(" AND ");
		if (hour == 24)
			ft_time(1);
		else
			ft_time(hour + 1);
		ft_putchar('\n');
	}
}
