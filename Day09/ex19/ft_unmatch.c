/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unmatch.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 16:01:49 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 16:03:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_unmatch(int *tab, int length)
{
	int	i;
	int	j;
	int	count;

	i = 0;
	while (i < length)
	{
		count = 0;
		j = 0;
		while (j < length)
		{
			if (tab[i] == tab[j])
				count++;
			j++;
		}
		if (count != 2)
			return (tab[i]);
		i++;
	}
	return (0);
}
