/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_door.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/16 14:30:05 by algrele           #+#    #+#             */
/*   Updated: 2018/02/16 14:54:41 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DOOR_H
# define FT_DOOR_H

# define OPEN 1
# define CLOSE 0
# define EXIT_SUCCESS 0
# include <unistd.h>

typedef	int		t_bool;

typedef struct	s_door
{
	t_bool		state;
}				t_door;

t_bool			close_door(t_door *door);
t_bool			open_door(t_door *door);
t_bool			is_door_close(t_door *door);
t_bool			is_door_open(t_door *door);

#endif
