/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   match.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 15:49:44 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 15:49:49 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		match(char *s1, char *s2)
{
	if (!*s1 && !*s2)
		return (1);
	else if (!*s1 && *s2 == '*')
		return (match(s1, s2 + 1));
	else if (!*s1 && *s2 != '*')
		return (0);
	else if (*s1 && !*s2)
		return (0);
	else if (*s1 != *s2 && *s2 != '*')
		return (0);
	else if (*s1 != *s2 && *s2 == '*')
		return ((match(s1, s2 + 1) || (match(s1 + 1, s2))));
	else if (*s1 == *s2 && *s1 != '*')
		return (match(s1 + 1, s2 + 1));
	else if (*s1 == *s2 && *s1 == '*')
		return (match(s1 + 1, s2));
	else
		return (0);
}
