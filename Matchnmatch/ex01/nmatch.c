/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nmatch.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 20:53:14 by algrele           #+#    #+#             */
/*   Updated: 2018/02/18 20:53:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		nmatch(char *s1, char *s2)
{
	if (!*s1 && !*s2 && *s1 == *s2)
		return (1);
	else if (!*s1 && *s2 == '*')
		return (nmatch(s1, s2 + 1));
	else if (!*s1 && *s2 != '*')
		return (0);
	else if (*s1 && !*s2)
		return (0);
	else if (*s1 != *s2 && *s2 != '*')
		return (0);
	else if (*s1 != *s2 && *s2 == '*')
		return (nmatch(s1, s2 + 1) + nmatch(s1 + 1, s2));
	else if (*s1 == *s2 && *s1 != '*')
		return (nmatch(s1 + 1, s2 + 1));
	else if (*s1 && *s2 == '*')
		return ((nmatch(s1 + 1, s2) + nmatch(s1, s2 + 1)));
	else
		return (0);
}
